<?php

namespace Terminalbd\GenericBundle\Form;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Entity\ParticularType;
use Terminalbd\GenericBundle\Repository\CategoryRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class CategoryFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true,'placholder'=>'Enter category name'],
                'required' => true
            ])
            ->add('glCode', TextType::class, [
                'attr' => ['autofocus' => true,'placholder'=>'Enter category code'],
                'required' => false
            ])
            ->add('config', HiddenType::class, [
                'attr' => ['autofocus' => true,'value' => $config],
                'required' => true
            ])
            ->add('itemMode',ChoiceType::class,
                array(
                    'attr'=>array('class'=>'col-md-4','placeholder'=>'Item mode'),
                    'required'    => true,
                    'choices'  => [
                        'OPEX' => 'OPEX',
                        'CAPEX' => 'CAPEX',
                        'Others' => 'Others',
                    ],
                )
            )
            ->add('parent', EntityType::class, [
                'class' => Category::class,
                'attr'=>['class'=>'select2'],
                'required' => false,
                'placeholder' => 'Choose a name',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['categoryRepo']->getFlatCategoryTree($options['config'])
            ])

            ->add('generalLedger', EntityType::class, [
                'class' => Head::class,
                'multiple' => false,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_label'  => 'nameWithCode',
                'attr'=>['class'=>'select2'],
                'placeholder' => 'Choose a item name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.level =4')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('size', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => true,
                'class' => ItemSize::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('color', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => true,
                'class' => ItemColor::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))
             ->add('brand', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => true,
                'class' => ItemBrand::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
            'categoryRepo' => CategoryRepository::class,
            'config' => GenericMaster::class,
        ]);
    }
}
