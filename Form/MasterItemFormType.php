<?php
namespace Terminalbd\GenericBundle\Form;


use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Entity\MasterItem;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Repository\CategoryRepository;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class MasterItemFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();

        $builder

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                'required'    => true,
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => false,'rows'=>3,'class'=>'textarea'],
                'label' => 'label.name',
                'required'    => false,

            ])
            ->add('priceMethod', EntityType::class, array(
                'required'    => true,
                'class' => Particular::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'span12 m-wrap'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->join("e.particularType","st")
                        ->where("st.slug ='price-method'")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('category', EntityType::class, [
                'class' => Category::class,
                'attr'=>['class'=>'select2'],
                'required' => false,
                'placeholder' => 'Choose a name',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['categoryRepo']->getFlatCategoryTree($options['config'])
            ])

            ->add('productGroup', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config){
                    return $er->createQueryBuilder('e')
                        ->join("e.particularType","st")
                        ->where("st.slug ='product-group'")
                        ->andWhere("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  product group',
            ])
            ->add('unit', EntityType::class, array(
                'required'    => false,
                'class' => ItemUnit::class,
                'placeholder' => 'Choose a  product unit',
                'choice_label' => 'name',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->orderBy('e.status', 'ASC');
                },
            ))
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MasterItem::class,
            'config' => GenericMaster::class,
            'categoryRepo' => CategoryRepository::class,
        ]);
    }
}
