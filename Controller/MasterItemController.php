<?php

namespace Terminalbd\GenericBundle\Controller;


use App\Repository\Application\GenericMasterRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\MasterItem;
use Terminalbd\GenericBundle\Form\MasterItemFormType;
use Terminalbd\GenericBundle\Repository\MasterItemRepository;
use Terminalbd\GenericBundle\Repository\ParticularRepository;


/**
 * @Route("/gmb/master-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class MasterItemController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     * @Route("/", methods={"GET", "POST"}, name="gmb_masteritem")
     */
    public function index(Request $request, TranslatorInterface $translator, ParticularRepository $particularRepository, GenericMasterRepository $masterRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $productTypes = $particularRepository->getChildRecords($config->getId(),"product-type");
        $productGroups = $particularRepository->getChildRecords($config->getId(),"product-group");
        $brands = $particularRepository->getChildRecords($config->getId(),"brand");
        $entity = new MasterItem();
        $data = $request->request->all();
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(MasterItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('data.created_successfully'));
            /*if(!empty($entity->getHsCode())){
                $this->getDoctrine()->getRepository('TerminalbdGenericBundle:MasterItem')->updateTaxValue($entity);
            }
            $this->getDoctrine()->getRepository('TerminalbdGenericBundle:ItemKeyValue')->masterInsertKeyValue($entity,$data);
            $this->getDoctrine()->getRepository('TerminalbdGenericBundle:ItemVat')->masterInsertSurcharge($entity,$data);*/
            return $this->redirectToRoute('gmb_masteritem');

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdGeneric/masteritem/index.html.twig', [
            'productTypes' => $productTypes,
            'productGroups' => $productGroups,
            'brands' => $brands,
            'categories' => '',
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="gmb_masteritem_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function edit(Request $request,$id, MasterItem $entity, TranslatorInterface $translator, GenericMasterRepository $masterRepository, MasterItemRepository $masterItemRepository, ParticularRepository $particularRepository): Response
    {

        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $productTypes = $particularRepository->getChildRecords($config->getId(),"product-type");
        $productGroups = $particularRepository->getChildRecords($config->getId(),"product-group");

        $entity = $masterItemRepository->findOneBy(array('config' => $config,'id' => $id));
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(MasterItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('data.created_successfully'));
            return $this->redirectToRoute('gmb_masteritem');

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdGeneric/masteritem/index.html.twig', [
            'entity' => $entity,
            'productTypes' => $productTypes,
            'productGroups' => $productGroups,
            'categories' => '',
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET", "POST"}, name="gmb_masteritem_show")
     * @Security("is_granted('ROLE_IINVENTORY_ITEM') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="gmb_masteritem_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_ITEM')")
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            return new Response('Data has been deleted successfully');

        } catch (ForeignKeyConstraintViolationException $e) {
            return new Response('Data has been relation another Table');
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="gmb_masteritem_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="gmb_masteritem_data_table", options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     */

    public function dataTable(Request $request , GenericMasterRepository $genericMasterRepository, MasterItemRepository $repository)
    {

        $query = $_REQUEST;

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);
        $iTotalRecords = $repository->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $repository->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):
            $active = empty($post['status']) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('gmb_masteritem_status',array('id'=>$post['id']))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $records["data"][] = array(
                $id                         = $i,
                $productGroupName           = $post['productGroupName'],
                $categoryName               = $post['categoryName'],
                $name                       = $post['name'],
                $unitName                   = $post['unitName'],
                $priceMethodName            = $post['priceMethodName'],
                $status                     = $status,
                $action                     ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_masteritem_edit',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:' data-action='{$this->generateUrl('gmb_masteritem_delete',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }


}
