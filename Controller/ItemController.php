<?php

namespace Terminalbd\GenericBundle\Controller;


use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Entity\MasterItem;
use Terminalbd\GenericBundle\Form\ItemFormType;
use Terminalbd\GenericBundle\Form\MasterItemFormType;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\GenericBundle\Repository\MasterItemRepository;
use Terminalbd\GenericBundle\Repository\ParticularRepository;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;


/**
 * @Route("/gmb/product-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ItemController extends AbstractController
{

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     * @Route("/", methods={"GET", "POST"}, name="gmb_item")
     */
    public function index(Request $request): Response
    {

        return $this->render('@TerminalbdGeneric/item/index.html.twig');
    }


    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     * @Route("/new", methods={"GET", "POST"}, name="gmb_item_new")
     */
    public function new(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository, ItemRepository $repository, InventoryRepository $inventoryRepository,StockBookRepository $stockBookRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $invConfig = $inventoryRepository->config($terminal);
        $entity = new Item();
        $data = $request->request->all();
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(ItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            echo $entity->getName();
            echo $count = $this->getDoctrine()->getRepository(Item::class)->count(['config'=>$config,'name' => $entity->getName()]);
            if($count == 0){
                $em = $this->getDoctrine()->getManager();
                if(empty($entity->getUnit())){
                    $pcs = $this->getDoctrine()->getRepository(ItemUnit::class)->findOneBy(array('slug'=>"pcs"));
                    $entity->setUnit($pcs);
                }
                $entity->setConfig($config);
                $em->persist($entity);
                $em->flush();
                $repository->insertStockItem($invConfig,$entity);
                $stockBookRepository->insertMasterStockItem($entity,$data);
                $this->addFlash('success',$translator->trans('data.created_successfully'));
                return $this->redirectToRoute('gmb_item_edit',array('id'=>$entity->getId()));
            }else{
                $this->addFlash('notice',"This product already created, Please try another item");
            }
        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdGeneric/item/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="gmb_item_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     */
    public function edit(Request $request,$id, TranslatorInterface $translator, GenericMasterRepository $masterRepository, ItemRepository $repository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(ItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo));
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('data.created_successfully'));
            return $this->redirectToRoute('inv_stock');

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdGeneric/item/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET", "POST"}, name="gmb_item_show")
     * @Security("is_granted('ROLE_GMB_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="gmb_item_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            return new Response('Data has been deleted successfully');
        } catch (ForeignKeyConstraintViolationException $e) {
            return new Response('Data has been relation another Table');
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="gmb_item_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_GMB_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-stcok-item", methods={"GET","POST"}, name="gmb_stcok_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_GMB_MANAGER') or is_granted('ROLE_GMB_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, Item $entity, ItemRepository $repository , StockBookRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $itemRepository->insertStockItem($entity,$data);
        $html = $this->renderView(
            '@TerminalbdGeneric/item/stock-book.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/stock-book-delete", methods={"GET"}, name="gmb_stcok_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_GMB_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function deleteStockItem(Item $entity , StockBook $item): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        try {
            $em->remove($item);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);

    }



    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="gmb_item_data_table", options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_GMB_MANAGER')")
     */

    public function dataTable(Request $request , GenericMasterRepository $genericMasterRepository,ItemRepository $repository)
    {

        $query = $_REQUEST;

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $genericMasterRepository->config($terminal);
        $iTotalRecords = $repository->count(array('config'=> $config));

        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc

        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder);

        $result = $repository->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):
            $active = empty($post['status']) ? '' : "checked";
            $status ="<input class='status' data-action='{$this->generateUrl('gmb_item_status',array('id'=>$post['id']))}' type='checkbox' {$active} data-toggle='toggle' data-size='xs' data-style='slow' data-offstyle='warning' data-onstyle='info' data-on='Enabled'  data-off='Disabled'>";
            $records["data"][] = array(
                $id                         = $i,
                $productGroupName           = $post['productGroupName'],
                $categoryName               = $post['categoryName'],
                $itemCode                   = $post['itemCode'],
                $name                       = $post['name'],
                $unitName                   = $post['unitName'],
                $purchasePrice              = $post['purchasePrice'],
                $salesPrice                 = $post['salesPrice'],
                $brand                      = $post['brand'],
                $status                     = $status,
                $action                     ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_item_edit',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:' data-action='{$this->generateUrl('gmb_item_delete',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }


}
