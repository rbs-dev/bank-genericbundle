function jqueryTemporaryLoad() {
    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    $('#item').select2('open');
    function jsonResult(response){
        obj = JSON.parse(response);
        $('#invoiceItem').html(obj['invoiceItem']);
        $('#subTotal').html(obj['subTotal']);
    }

    var count = 0;
    var countId = 1;
    var countIdx = 1;
    $('.addFile').click(function(e){
        var $el = $(this);
        var $cloneBlock = $('#clone-block');
        var $clone = $cloneBlock.find('.clone:eq(0)').clone();
        $clone.find('.custom-file-input').attr('id', "file-"+(countId++));
        $clone.find('.custom-file-label').attr('for', "file-"+(countIdx++));
        $clone.find(':text,textarea,file' ).val("");
        $clone.attr('id', "added"+(++count));
        $clone.find('.row-remove').removeClass('hidden');
        //    $clone.find('.custom-file-label').html('');
        $cloneBlock.append($clone);
    });


    $('#sortable').sortable({

        update: function(event, ui) {
            $('#sortable').children().each(function(i) {
                var id = $(this).attr('data-post-id')
                    ,order = $(this).index() + 1;
                $.get(Routing.generate('procure_jobrequisition_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $('#sortableRequsition').sortable({
        update: function(event, ui) {
            $('#sortableRequsition').children().each(function(i) {
                var id = $(this).attr('data-post-id')
                    ,order = $(this).index() + 1;
                $.get(Routing.generate('procure_requisition_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $('#sortableRequsitionIssue').sortable({
        update: function(event, ui) {
            $('#sortableRequsition').children().each(function(i) {
                var id = $(this).attr('data-post-id')
                    ,order = $(this).index() + 1;
                $.get(Routing.generate('procure_requisition_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $('#sortableRequsitionOrder').sortable({
        update: function(event, ui) {
            $('#sortableRequsition').children().each(function(i) {
                var id = $(this).attr('data-post-id')
                    ,order = $(this).index() + 1;
                $.get(Routing.generate('procure_requisition_ordering'),{'columnId':id,'sorting':order})});

        }
    });

    $("#itemGmbForm").validate({
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                var firstInvalidElement = $(validator.errorList[0].element);
                $('html,body').scrollTop(firstInvalidElement.offset().top);
            }
        }
    });


    $("#submitBtn").click(function(){

        var sum = 0;
        $(".shareParcent").each(function(){
            amount = ($(this).val() === "" || $(this).val() === "NaN") ? 0 : $(this).val();
            sum += +parseFloat(amount);
        });
        if (sum > 100 || (sum !== 0 && sum < 100)) {
            alert("Please confirm company share ratio");
            return false;
        }
        $("#itemGmbForm").valid();
        $(".main-form").submit()
    });

    $(document).on('change', '.itemName', function() {

        var id = $(this).val();
        $.ajax({
            url: Routing.generate('inv_category_attribute'),
            type: 'POST',
            data:'id='+ id,
            success: function(response) {
                obj = JSON.parse(response);
                $(".brand").html(obj['brand']);
                $(".size").html(obj['size']);
                $(".color").html(obj['color']);
            }
        })

    });

    $(document).on('change', ".shareParcent", function() {
        var sum = 0;
        $(".shareParcent").each(function(){
            amount = ($(this).val() === "" || $(this).val() === "NaN") ? 0 : $(this).val();
            sum += +parseFloat(amount);
        });
        if (sum > 100) {
            alert("Please confirm company share ratio");
        }
    });
    /*$('form#itemForm').on('keypress', '.input', function (e) {

        if (e.which === 13) {
            var inputs = $(this).parents("form#itemForm").eq(0).find("input,select");
            var idx = inputs.index(this);
            if (idx == inputs.length - 1) {
                inputs[0].select()
            } else {
                inputs[idx + 1].focus(); //  handles submit buttons
            }
            switch (this.id) {
                case 'item':
                    $('#quantity').focus();
                    break;

                case 'quantity':
                    var qnt = $('#quantity').val();
                    if(qnt == "NaN" || qnt =="" ){
                        $('#quantity').focus();
                    }else{
                        $('#description').focus();
                    }
                    break;

                case 'description':
                    $('#addItem').click();
                    $('#purchaseItem_stockName').select2('open');
                    break;

            }
            return false;
        }
    });*/

    $('form#approveForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#approveForm').attr( 'action' ),
            type        : $('form#approveForm').attr( 'method' ),
            data        : new FormData($('form#approveForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    $('form#itemForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('form#itemForm')[0].reset();
                $('#stockBookItem').html(response);
            }
        });
    });

    $('form#additionalItemForm').submit(function(e){

        e.preventDefault();
        var item = $('#job_requisition_additional_item_form_name').val();
        var quantity = $('#job_requisition_additional_item_form_quantity').val();

        if(item === "NaN" || item ==="" ){
            $('#name').focus();
            return false;
        }
        if(quantity === '' || quantity === "NaN"){
            $('#quantity').focus();
            return false;
        }

        $.ajax({
            url         : $('form#additionalItemForm').attr( 'action' ),
            type        : $('form#additionalItemForm').attr( 'method' ),
            data        : new FormData($('form#additionalItemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                obj = JSON.parse(response);
                $('#invoiceAdditionalItem').html(obj['invoiceItem']);
                $('form#additionalItemForm')[0].reset();
            }
        });
    });

    $('form#addApproval').submit(function(e){

        e.preventDefault();
        var item = $('#assignUser').val();
        if(item === "NaN" || item ==="" ){
            $('#assignUser').select2('open');
            return false;
        }
        $.ajax({
            url         : $('form#addApproval').attr( 'action' ),
            type        : $('form#addApproval').attr( 'method' ),
            data        : new FormData($('form#addApproval')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('.status').bootstrapToggle();
                $('.approvalUser').html(response);
            }
        });
    });

    $(document).on('change', '.quantity', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var stockIn = parseFloat($('#stockIn-'+id).val());
        var salesPrice = parseFloat($('#salesPrice-'+id).val());
        var subTotal  = (quantity * salesPrice);
        $("#subTotal-"+id).html(subTotal);
        $.ajax({
            url: Routing.generate('procure_requisition_item_update'),
            type: 'POST',
            data:'item='+ id +'&quantity='+quantity +'&stockIn='+stockIn,
            success: function(response) {
                setTimeout(jsonResult(response),100);
            }
        })
    });

    $(document).on('keypress keyup blur', '.storeRequisitionQuantity', function() {
        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var requisitionItemPrice = parseFloat($('#requisitionItemPrice-'+id).val());
        var remainingQuantity = parseFloat($('#remainingQuantity-'+id).val());
        var remin  = (remainingQuantity - quantity);
        var subTotal = quantity*requisitionItemPrice;
        $("#remainQnt-"+id).html(remin);
        $("#subTotal-"+id).html(subTotal);

        var sum = 0;
        $(".subTotal").each(function(){
            sum += +parseFloat($(this).text());
        });
        $(".grandTotal").html(sum);
    });

    $(document).on('click',".itemRemove", function (event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                if(response === 'invalid'){
                    location.reload();
                }else{
                    $(event.target).closest('tr').hide();
                    setTimeout(jsonResult(response),100);
                }
            });
        });
    });

    $(document).on('click',".itemApprovalRemove", function (event) {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                if(response === 'invalid'){
                    location.reload();
                }else{
                    $(event.target).closest('tr').hide();
                }
            });
        });
    });

    $(".autocomplete2Item").select2({
        ajax: {
            url: Routing.generate('inv_stock_item_autocomplete'),
            data: function (params, page) {
                return {
                    q: params,
                    page_limit: 100
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        },
        placeholder: 'Search for a add stock item',
        minimumInputLength: 1,
    });

    $(document).on('change', '.autocomplete2Item', function () {
        var employee = $(this).val();
        alert(employee);
    });
}






