$(document).ready(function () {
    var table = $('#datatable').DataTable( {
        "ajax": {
            "type"   : "POST",
            "processing": true,
            "serverSide": true,
            "url": Routing.generate('gmb_bank_category_data_table')
        },
        "initComplete": function() {
            $('.status').bootstrapToggle()
        },
        columnDefs: [{orderable: false,targets:2},{orderable: false,targets:3}],
       // pageLength: 25,
        scrollY:'100vh',
        scrollCollapse: true,
        "paging": false,
        order: [ 0, 'asc' ]
    });

});

