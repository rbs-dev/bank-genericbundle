<?php

namespace Terminalbd\GenericBundle\Entity;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\Admin\Location;
use Terminalbd\GenericBundleBundle\Repository\ParticularRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\GenericBundle\Repository\ParticularRepository")
 * @ORM\Table(name="gmb_particular")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Particular
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var GenericMaster
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\GenericMaster")
     */
    private $config;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Particular", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $parent;


    /**
     * @var ParticularType
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ParticularType")
     */
    private $particularType;

     /**
     * @var ParticularType
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\GenericBundle\Entity\Particular", mappedBy="parent")
     */
    private $children;

    /**
     * @var ItemKeyValue
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\GenericBundle\Entity\ItemKeyValue" , mappedBy="particular")
     */
    private $itemKeyValues;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $code;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $nameBn;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false, nullable=true)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getNameBn()
    {
        return $this->nameBn;
    }

    /**
     * @param string $nameBn
     */
    public function setNameBn($nameBn)
    {
        $this->nameBn = $nameBn;
    }

    public function getNameBanglaEnglish(){

        return $this->nameBn.' - '.$this->name;
    }

    /**
     * @return ParticularType
     */
    public function getParticularType()
    {
        return $this->particularType;
    }

    /**
     * @param ParticularType $settingType
     */
    public function setParticularType(ParticularType $settingType)
    {
        $this->particularType = $settingType;
    }

    /**
     * @return ItemKeyValue
     */
    public function getItemKeyValues()
    {
        return $this->itemKeyValues;
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return GenericMaster
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param GenericMaster $config
     */
    public function setConfig(GenericMaster $config)
    {
        $this->config = $config;
    }


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $mobile;

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactName;

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;
    }

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $contactPhone;

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;
    }

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $email;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $postalCode;

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $additionalPhone;

    /**
     * @return string
     */
    public function getAdditionalPhone()
    {
        return $this->additionalPhone;
    }

    /**
     * @param string $additionalPhone
     */
    public function setAdditionalPhone($additionalPhone)
    {
        $this->additionalPhone = $additionalPhone;
    }


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $billingAddress;

    /**
     * @return string
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param string $billingAddress
     */
    public function setBillingAddress(string $billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $address;

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return Particular
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Particular $parent
     */
    public function setParent($parent): void
    {
        $this->parent = $parent;
    }




}
