<?php

namespace Terminalbd\GenericBundle\Entity;

use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Repository\ItemKeyValueRepository;


/**
 * ItemKeyValueRepository
 *
 * @ORM\Table(name="gmb_key_value")
 * @ORM\Entity(repositoryClass="Terminalbd\GenericBundle\Repository\ItemKeyValueRepository")
 */
class ItemKeyValue
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var Particular
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Particular", inversedBy="itemKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $particular;

    /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item", inversedBy="itemKeyValues" )
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $item;

    /**
     * @var string
     * @ORM\Column(name="metaKey", type="string", length=255, nullable = true)
     */
    private $metaKey;

    /**
     * @var string
     *
     * @ORM\Column(name="metaValue", type="string", length=255 , nullable = true)
     */
    private $metaValue;

    /**
     * @var Integer
     *
     * @ORM\Column(name="sorting", type="smallint", length=2, nullable = true)
     */
    private $sorting;

    /**
     * @var Integer
     *
     * @ORM\Column(type="smallint", length=2, nullable = true)
     */
    private $inputType;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue($metaValue)
    {
        $this->metaValue = $metaValue;
    }


    /**
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param int $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }


    /**
     * @return Particular
     */
    public function getParticular()
    {
        return $this->particular;
    }

    /**
     * @param Particular $particular
     */
    public function setParticular($particular)
    {
        $this->particular = $particular;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getInputType()
    {
        return $this->inputType;
    }

    /**
     * @param int $inputType
     */
    public function setInputType($inputType)
    {
        $this->inputType = $inputType;
    }






}

